---
title: 'GitLab Pages'
subtitle: 'GitLab'
authors: ['Per Dahlstrøm \<pda@ucl.dk>']
main_author: 'Per Dahlstrøm'
date: \today
email: 'pda@ucl.dk'
left-header: \today
right-header: 'GitLab Pages'
---

# Audience

Students and lectureres at UCL.

---

# Purpose

The purpose of this document is to provide students and Lecturers an entry level guide to GitLab pages and for me Per dahlstrøm to take notes on what I am doing.

---

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

# jekyll  

In order to make the GitLab md documents into a Web Site this seems to be a good starting point.

https://github.com/pmarsceill/just-the-docs



# iot-with-particle

This is Nikolaj and Mortens take on the 2 semester iot-with-particle project spring 2020:

https://gitlab.com/EAL-ITT/iot-with-particle/-/tree/master

# Starting my project to make project Networking into a WebSite

Out fear of destroying Networking repository I have exported it and created a new project NetworkExport and imported Networking.



Following the guide:

[https://eal-itt.gitlab.io/discover-iot/cloud/gitlab-pages](https://eal-itt.gitlab.io/discover-iot/cloud/gitlab-pages)

## Install mkdocs in your gitlab project

1. create a virtual environment in your gitlab project (if you already have one use that)  
 https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment  
 PDA:  
 
   1. On windows open a bash prompt in the project.  

pda@EAL-01189 MINGW64 /c/UCL/Projekter/1 semester/Networking/GitLabPages
$ git clone git@gitlab.com:PerPer/networking.git
Cloning into 'networking'...

pda@EAL-01189 MINGW64 /c/UCL/Projekter/1 semester/Networking/GitLabPages
$ cd networking/

pda@EAL-01189 MINGW64 /c/UCL/Projekter/1 semester/Networking/GitLabPages/networking (master)
$ py -m pip install --upgrade pip
Successfully installed pip-21.2.4

Install the virtual environment "generator" on your windows system:

pda@EAL-01189 MINGW64 /c/UCL/Projekter/1 semester/Networking/GitLabPages/networking (master)
$ py -m pip install --user virtualenv
Collecting virtualenv
  WARNING: The script virtualenv.exe is installed in 'C:\Users\pda\AppData\Roaming\Python\Python37\Scripts' which is not on PATH.
  Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.
Successfully installed backports.entry-points-selectable-1.1.0 distlib-0.3.3 filelock-3.3.0 importlib-metadata-4.8.1 platformdirs-2.4.0 six-1.16.0 typing-extensions-3.10.0.2 virtualenv-20.8.1 zipp-3.6.0

pda@EAL-01189 MINGW64 /c/UCL/Projekter/1 semester/Networking/GitLabPages/networking (master)
$

To create a virtual environment, go to your project’s directory and run venv. If you are using Python 2, replace venv with virtualenv in the below commands.  

pda@EAL-01189 MINGW64 /c/UCL/Projekter/1 semester/Networking/GitLabPages/networking (master)
$ py -m venv env

Note You should exclude your virtual environment directory from your version control system using .gitignore or similar.



2. activate the environment  
 https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#activating-a-virtual-environment  

Before you can start installing or using packages in your virtual environment you’ll need to activate it. Activating a virtual environment will put the virtual environment-specific python and pip executables into your shell’s PATH.

Go to windows command prompt to activate the virtual environment:

C:\Users\pda>cd "\UCL\Projekter\1 semester\Networking\GitLabPages\networkingexport"

Call the activate script:

C:\UCL\Projekter\1 semester\Networking\GitLabPages\networkingexport>.\env\Scripts\activate

The prompt will change to this with (env) added:

(env) C:\UCL\Projekter\1 semester\Networking\GitLabPages\networkingexport>

Power shell not permission:
.\env\Scripts\activate : File C:\UCL\Projekter\1 semester\Networking\GitLabPages\networkingexport\env\Scripts\Activate.ps1 cannot be loaded because running scripts is disabled on this system.

Use the UNIX command shown in the guide to activate the virtual environment in bash.  

3. Follow the guide at  
 https://www.mkdocs.org/#installing-mkdocs  
  start from installing mkdocs  
! do not follow the deploy part at the end of the guide as it doesn’t cover gitlab pages, continue this guide when you are satisfied with your page content and layout.  

I do the installation in windows command prompt :-()




4. Move the folder docs and the file mkdocs.yml to your project and place it in a folder called pages  


A take 12-10-2021 on doing it. :-)

pda@EAL-01189 MINGW64 /c/UCL/Projekter/1 semester/Networking/GitLabPages/NetworkingPages  
$ git clone git@gitlab.com:PerPer/networkingpages.git  
Cloning into 'networkingpages'...  
Receiving objects: 100% (3/3), done.   

pda@EAL-01189 MINGW64 /c/UCL/Projekter/1 semester/Networking/GitLabPages/NetworkingPages
$ cd networkingpages/  

pda@EAL-01189 MINGW64 /c/UCL/Projekter/1 semester/Networking/GitLabPages/NetworkingPages/networkingpages (main)  
$ py -m venv env  

pda@EAL-01189 MINGW64 /c/UCL/Projekter/1 semester/Networking/GitLabPages/NetworkingPages/networkingpages (main)  
$ source env/Scripts/activate  

(env)  
pda@EAL-01189 MINGW64 /c/UCL/Projekter/1 semester/Networking/GitLabPages/NetworkingPages/networkingpages (main)  

(env)  
pda@EAL-01189 MINGW64 /c/UCL/Projekter/1 semester/Networking/GitLabPages/NetworkingPages/networkingpages (main)  
$ which python  
/c/UCL/Projekter/1 semester/Networking/GitLabPages/NetworkingPages/networkingpages/\UCL\Projekter\1 semester\Networking\GitLabPages\NetworkingPages\networkingpages\env/Scripts/python  

(env)  
pda@EAL-01189 MINGW64 /c/UCL/Projekter/1 semester/Networking/GitLabPages/NetworkingPages/networkingpages (main)  
$ python -m pip install --upgrade pip  

(env)  
pda@EAL-01189 MINGW64 /c/UCL/Projekter/1 semester/Networking/GitLabPages/NetworkingPages/networkingpages (main)  
$ pip install mkdocs  
Collecting mkdocs  

pda@EAL-01189 MINGW64 /c/UCL/Projekter/1 semester/Networking/GitLabPages/Networ
$ mkdocs new my-project
INFO     -  Creating project directory: my-project
INFO     -  Writing config file: my-project\mkdocs.yml
INFO     -  Writing initial docs: my-project\docs\index.md
(env)
pda@EAL-01189 MINGW64 /c/UCL/Projekter/1 semester/Networking/GitLabPages/NetworkingPages/networkingpages (main)
$ cd my-project  

(env)
pda@EAL-01189 MINGW64 /c/UCL/Projekter/1 semester/Networking/GitLabPages/NetworkingPages/networkingpages/my-project (main)
$

(env)  
pda@EAL-01189 MINGW64 /c/UCL/Projekter/1 semester/Networking/GitLabPages/NetworkingPages/networkingpages/my-project (main)  
$ mkdocs build  
INFO     -  Cleaning site directory  


