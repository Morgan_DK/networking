---
title: 'Docker demonstrated'
subtitle: 'Docker demonstrated'
authors: ['Per Dahlstrøm \<pda@ucl.dk>']
main_author: 'Per Dahlstrøm'
date: \today
email: 'pda@ucl.dk'
left-header: \today
right-header: 'Docker demonstrated'
---

# Audience

Students at the two year IT Technology education at UCL in Odense Denmark.

---

# Purpose

A quick demonstartion on using Docker. The demonstration is made so that it can be followed, i.e. replicated. and in this way get the audience started with using Docker.

---

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

# Example network

This example network diagram serves as the foundation for describing network communication with containers.

The network consists of two routes RL(inux) and R3. RL and R3 are interconnected through network 10.56.16.0/22. This latter network is the UCL school network.

The RL stands for RLinux as this is the router functionality inside the Linux box running Docker.


![Semantic description of image](Docker_network_diagram.JPG "Generic network diagram") 

# Hardware

In this demonstation Docker is installed and run on a Xubuntu VM in VMWare Workstation.

---

# Installation

Do an `apt get update` and an `apt get upgrade` before installing Docker.

Open a browser on the Xubuntu machine and go to https://get.docker.com/

At this URL a script will be displayed. Follow the instructions in the displayed script:

Fetch the script from the URL by curl:  

$ `curl -fsSL https://get.docker.com -o get-docker.sh`

Check the authenticity of the script: SCRIPT_COMMIT_SHA="f45d7c11389849ff46a6b4d94e0dd1ffebca32c1"

![Semantic description of image](The_install_script.JPG "Generic network diagram")  
* Figure 1 The_install_script.JPG* 


Run the just downlaoded script:

$ `sh get-docker.sh`  

*Docker will install into the linux apt system(?) and not as a binary in som obscure directory?*  

If you would like to use Docker as a non-root user, you should now consider adding your user to the "docker" group:

  `sudo usermod -aG docker per`  Here for now user x, here per, is NOT added to the docker group.

Remember to to log out and back in for this to take effect!

Start the docker service:

per@ubuntu:~$ `sudo service docker start`  

per@ubuntu:~$ `sudo docker version`  

Client: Docker Engine - Community 
      Version:           19.03.5  
      API version:       1.40  
      Go version:        go1.12.12  
      Git commit:        633a0ea838  

This ends the installation

---

# Dockerize an app and run it in docker

The first test application is going to be a bash script that counts from 1 to infinity in1 second intervals.

$ `/bin/bash -c 'while true; do X=$[X+1]; echo $X; sleep 1; done'`

Copy this and run it in a terminal.

![Semantic description of image](First_app_simple_bash_script.JPG "Generic network diagram")  
* Figure 1 First_app_simple_bash_script.JPG*

Create a centos docker container and run the app as a deamon (-d):

$ `sudo docker run -d centos /bin/bash -c 'while true; do X=$[X+1]; echo $X; sleep 1; done'`

![Semantic description of image](Creating_centos_docker_for_app.JPG "Generic network diagram")  
* Figure 1 Creating_centos_docker_for_app.JPG*

run will execute the command given to it in the specified environment, in this case centos. Run will look in the local image reposetory for centos. If it is not there, it will be downloade from docker.hub.

This container is instructed to run the command: `/bin/bash -c 'while true; do X=$[X+1]; echo $X; sleep 1; done`

It is not the centos virtual machine that is downloaded. Only dependencies and other things neede to run a container.

Check if the app is runnuing:

$ `sudo docker ps`

To see the output run the docker log:

$ `sudo docker logs 309`  

The number can be the first 3-6 digits of the container ID or use the funny name given to it.

$ `sudo docker stop 309` 

$ `sudo docker start 309` 

![Semantic description of image](Docker_log_output_start_and_stop.JPG "Generic network diagram")  
* Docker_log_output_start_and_stop.JPG*



---

# Managing images

List images on this machine:

$ `sudo docker images`  

![Semantic description of image](Docker_images.JPG "Generic network diagram")  
* Docker_images.JPG*

Search for a speciific image. In this case search for the php image:

$ `sudo docker search php`  

If the image is nor found on the machine, docker automatically serarch on Docker-Hub.

Locate the name of the desired image and pull it from the repository:

$ `sudo docker pull php`

![Semantic description of image](Docker_pull_image.JPG "Generic network diagram")  
* Docker_pull_image.JPG*

An other container image example is Ubuntu:

$ `sudo docker pull ubuntu`

Delete a container image:

$ `sudo docker rmi php`

![Semantic description of image](Docker_pull_ubuntu_delete_php.JPG "Generic network diagram")  
* Docker_pull_ubuntu_delete_php.JPG*



---

# Managing containers with ps

Start and stop container:

$ `sudo docker stop 309` 

$ `sudo docker start 309` 

The number can be the first 3-6 digits of the container ID or use the funny name given to it.

See what conatiners are on the system

$ `sudo docker ps -a`

Delete or remove a container:

$ `sudo docker rm 309`

or 

$ `sudo docker rm funny_proskuriakova`

Removing a container will leave the images untouched. And images should be left alone except updated when necessary. Images are sitting there on the system ready to be pulled or copied into containers.

---

# Create and run a Python app or program in a docker container



Create a directory my_dockers with two files in it. Dockerfile and my_script.py.

![Semantic description of image](Docker_my_docker_directory.JPG "Generic network diagram")  
* Docker_my_docker_directory.JPG*

Source:  
https://runnable.com/docker/python/dockerize-your-python-application

Create the Dockerfile:

![Semantic description of image](Docker_the_Dockerfile.JPG "Generic network diagram")  
* Docker_the_Dockerfile.JPG*



`FROM python:3`  
`ADD my_script.py /`  
`CMD [ "python", "./my_script.py" ]`

Create the python script:

`print('Hello from Per Dahlstrøm. UCL')`

Create or build the dDocker image:

per@ubuntu:~/my_dockers$ `sudo docker build -t my_python_name_image .` Note the dot!

Run the docker container:

per@ubuntu:~/my_dockers$ `sudo docker run my_python_name_image`

![Semantic description of image](Docker_python_output.JPG "Generic network diagram")  
* Docker_python_output.JPG*

---

# Networking part 1

For this initial networking instruction a simple docker image which only purpose is to run a webserver that serve a web page on port 8080, will be used. Its name is adejonge/helloworld.

By running ifconfig and route -n it is shown that the docker instalation created a virtual interface named docker0 attached to the network 172.17.0.0/16. The interface IP address is 172.17.0.1, which is seen by running ip addr or ifconfig.

![Semantic description of image](Docker_network_route.JPG "Generic network diagram")  

To create the bridge between the docker container and the 172.17.0.0 network, pull and install the container image adejonge/helloworld. All the magic will happen in one go.

$ `sudo docker run -d adejonge/helloworld`

Verify that the container image is running. Here it jhas been running for 16 hours and has been give the funny name: sharp_goldwasser. That name is use further below here.

![Semantic description of image](Docker_adejonge_running.JPG "Generic network diagram") 

A virtual bridge to the container network interface docker0 named veth618ca27@if24 was created as the adejonge/helloworld image container was created. Run `ip addr` to verify:

![Semantic description of image](Docker_container_virtual_interface.JPG "Generic network diagram") 

As shown in the diagram below, docker creates an interface in the container, with an IP address, on the docker0/bridge network. Find the container virtual interface IP address:

per@ubuntu:~/my_dockers$ `sudo docker inspect sharp_goldwasser | grep IPAdd`

![Semantic description of image](Docker_inspect_container.JPG "Generic network diagram")  

![Semantic description of image](Docker_network_diagram.JPG "Generic network diagram")

The webserver serves a page on http://172.17.0.2:8080.

![Semantic description of image](Docker_Webserver_adejonge.JPG "Generic network diagram") 

Yet another webserver can be spun up:

$ `sudo docker run -d adejonge/helloworld`


![Semantic description of image](Docker_adejonge_number_2_running.JPG "Generic network diagram")

The IP address of the new container is found as before:

$ `sudo docker inspect sharp_goldwasser | grep IPAdd`

And the topology now is with two webservers. Try accessing the new web server as before.

![Semantic description of image](Docker_network_diagram_2_adjonge_webservers.JPG "Generic network diagram")

WOW did you appreciate how fast it was to spin up a new webserver?

---

# Networking part 2 Expose containers to the outside world

The aim here is to expose the two webservers to network 10.56.16.0, so that e.g. PC1 at 10.56.16.86 can access the weppages on the two containers at 172.17.0.2 and .3. This is done by setting up port mapping or port forwarding when creating the containers.

Here two containers with forwarding to port 8080 from two ports are created. It is of course possible to just forward from one port.

$ `sudo docker run -d -p 8000:8080 -p 8001:8080 adejonge/helloworld`

$ `sudo docker run -d -p 9000:8080 -p 9001:8080 adejonge/helloworld`


![Semantic description of image](Docker_network_diagram_port_mapping.JPG "Generic network diagram")

$ `sudo  docker ps`

CONTAINER ID        IMAGE                 COMMAND             CREATED             STATUS  
f0f65f0a58d8        adejonge/helloworld   "/helloworld"       4 seconds ago       Up 3 seconds  
7dee6a29dd4c        adejonge/helloworld   "/helloworld"       22 seconds ago      Up 21 seconds  

PORTS                                            NAMES  
0.0.0.0:9000->8080/tcp, 0.0.0.0:9001->8080/tcp   priceless_feynman  
0.0.0.0:8000->8080/tcp, 0.0.0.0:8001->8080/tcp   jolly_pascal

![Semantic description of image](Docker_port_forwarding_ports.JPG "Generic network diagram")

On PC1 at 10.56.16.86 the webservers and thus their webpages can now be reached at 10.56.16.85:8000 and port 8001 and  port 9000 and port 9001. PC1 is here my XubuntuBase VM but that is unimportant.

![Semantic description of image](Docker_port_forwarding_acces_from_outside.JPG "Generic network diagram")

Verify that the host computer is listening for incomming connetions on ports 8000 8001 and 9000 9001:

![Semantic description of image](Docker_port_forwarding_netstat_grep_LISTENING.JPG)

The tcp6 LISTEN entries are for the sockets listening on ports 8000, 8001 and 9000, 9001. It is (probably) an AF_INET6 socket, so it will accept both IPv4 and IPv6 incoming connections.

# Networking part 3 Expose containers to the outside world

To simply just use the hosts IP address on the container(s?) use the --net switch:

$ `sudo docker run -d --net=host adejonge/helloworld`

![Semantic description of image](Docker_network_diagram_--net-host.JPG)

Now the webserver and its pages can reached at 10.56.16.85:8080.

![Semantic description of image](Docker_--net-host_acces_from_outside.JPG)

As the container is sharing the hosts IP address, no IP address is showing up in:

$ `sudo docker inspect tender_napier | grep IP`

It looks like another container can not operate on the hosts IP and on the used port. Maybe it is possible to run another container on the hosts IP on another port? Remains to be investigated.

# Data storage

As containers are dispensabel, data should not be stored in containers. Here it will be demonstarted how to map directories in the container to directories on the host computer.

For this instruction the web server nginx image docker container will be spun up and data will be stored for the web server in the form of a index.html web page in a host computer directory.

Nginx serves its index.html page from the location: /usr/share/nginx/html

Here the host folder /home/per/my_dockers/web will thus be mapped to /usr/share/nginx/html

$ `sudo docker run -d -v /home/per/my_dockers/web:/usr/share/nginx/html nginx`

or if a more memorable name should be used for the container:

$ `sudo docker run -d -v /home/per/my_dockers/web:/usr/share/nginx/html --name pda_nginx nginx`




Now the content of the host directory /home/per/my_dockers/web substitutes the nginx containers /usr/share/nginx/html directory.

![Semantic description of image](Docker_nginx_directory_mapping.JPG)

Create a index.html page in /home/per/my_dockers/web

![Semantic description of image](Docker_nginx_index_html_page.JPG)

$ `sudo docker ps`

$ `sudo docker inspect pda_nginx | grep IP`

![Semantic description of image](Docker_network_diagram_nginx_data_store.JPG)

Fetch the page form the IP learned right above here:

![Semantic description of image](Docker_Custom_page_Nginx.JPG)









---

# Modifying containers

Spinn up a nginx image container. Here it is demonstrated how to change the image in a container and then generating or saving a new image from this modified image.

$ `sudo docker run -d --name pda_nginx nginx`

Verify that the webserver image container is running and functioning:

![Semantic description of image](Docker_Welcome_Nginx.JPG)

**To enter the container to modify it:**

$ `sudo docker exec -i -t pda_nginx bash`  

-i: interactive  
-t: terminal  
bash: the command that should be executed  

![Semantic description of image](Docker_entering_bash.JPG)

This brought up the command line in the shell for this container. Here some of the usual Linux commands can be used. But not e.g. the networking commads like ping and route.

Change directory into the nginx html directory and modify the index.html file:

\# `cd /usr/share/nginx/html`

\# `cat index.html`

\# `rm index.html`

\# `cat > index.html`

Enter someting meaning full into the webpage: `Hello from inside the container.`

Finish by ctrl + d.

Check in the browser that the page has changed at 172.17.0.2:80.

\# `exit`

**To now create an image from the running container do this:**

$ `sudo docker commit pda_nginx my_new_nginx`

This new image can be seen in the list of images on the computer:

![Semantic description of image](Docker_my_new_nginx_image.JPG)

Delete all containers and spin up this brand ne image container:

$ `sudo docker run -d my_new_nginx`

Verify that it is serving the expected page in the browser again.

![Semantic description of image](Docker_hello_from_inside_container.JPG)

As an exercise: Create a nginx container with an external store or directory and make a new image from it. Check that i works as expected fetching index.html from the external directory.


# Create image from scratch

To be continued.
