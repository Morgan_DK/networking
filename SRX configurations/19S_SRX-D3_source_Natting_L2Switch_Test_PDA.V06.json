## Last commit: 2019-02-09 14:25:16 UTC by root
version 12.1X46-D30.2;
system {
    host-name vSRX-NAT;
    root-authentication {
        encrypted-password "$1$xH9xJoL6$MFOUYnZr4.Qj2NM24XInz/"; ## SECRET-DATA
    }
    services {
        ssh;
        web-management {
            http {
                interface ge-0/0/0.0;
            }
        }
        dhcp {
            router {
                192.168.3.1;
            }
            pool 192.168.3.0/24 {
                address-range low 192.168.3.10 high 192.168.3.20;
                maximum-lease-time 3600;
                default-lease-time 3600;
                name-server {
                    8.8.8.8;
                }
            }
        }
    }
    license {
        autoupdate {
            url https://ae1.juniper.net/junos/key_retrieval;
        }
    }
}
interfaces {
	/* L2 switch */
    interface-range USERLAN-switch {
        member ge-0/0/10;
        member ge-0/0/11;
        member ge-0/0/12;
        member ge-0/0/13;
        unit 0 {
            family ethernet-switching {
                vlan {
                    members vlan-USERLAN;
                }
            }
        }
    }
    ge-0/0/1 {
        unit 0 {
            family inet {
                /* USERLAN */
                address 192.168.3.1/24;       
            }
        }
    }
    ge-0/0/2 {
        /* Interface for DMZ */
        unit 0 {
            family inet {
				/* Port mirror */
                filter {
                    input my-port-filter;
                    output my-port-filter;
                }
                }
                /* DMZ */
                address 192.168.4.1/24;
            }
        }
    }
    ge-0/0/3 {
        unit 0 {
            family inet {
                /* schools network */
                address 10.217.16.200/22;
            }
        }
    }
    ge-0/0/4 {
        unit 0 {
            family inet {
                address 2.2.2.2/24;
            }
        }
    }
    /* DMZ L2 switch */
    ge-0/0/5 {
        unit 0 {
            family ethernet-switching {
                vlan {
                    members vlan-DMZ;
                }
            }
        }
    }
    ge-0/0/6 {
        unit 0 {
            family ethernet-switching {
                vlan {
                    members vlan-DMZ;
                }
            }
        }
    }
    ge-0/0/7 {
        unit 0 {
            family ethernet-switching {
                vlan {
                    members vlan-DMZ;
                }
            }
        }
    }
    vlan {
        unit 0 {
            family inet;
        }
    }
}
/* port mirroring */
forwarding-options {
    port-mirroring {
        input {
            rate 1;
            run-length 10;
        }
        family inet {
            output {
                interface ge-0/0/4.0 {
                    next-hop 2.2.2.1;
                }
            }
        }
    }
}
routing-options {
    /* Default route to school gateway */
    static {
        route 0.0.0.0/0 next-hop 10.217.16.1;
    }
}
security {
    nat {
        source {
            rule-set DMZ-to-untrust {
                from zone DMZ;
                to zone untrust;
                rule rule-any-to-any {
                    match {
                        source-address 0.0.0.0/0;
                        destination-address 0.0.0.0/0;
                    }
                    then {
                        source-nat {
                            interface;
                        }
                    }
                }
            }
            rule-set trust-to-untrust {
                from zone trust;
                to zone untrust;
                rule DMZ-rule-any-to-any {
                    match {
                        source-address 0.0.0.0/0;
                        destination-address 0.0.0.0/0;
                    }
                    then {
                        source-nat {
                            interface;
                        }
                    }
                }
            }
        }
        destination {
            pool ApacheWebServer {
                address 192.168.4.10/32;
            }
            rule-set DestinationNATRuleWebServer {
                from zone untrust;
                rule ruleToWebServer {
                    match {
                        destination-address 10.217.16.200/32;
                        destination-port 80;
                    }
                    then {
                        destination-nat {
                            pool {
                                ApacheWebServer;
                            }
                        }
                    }
                }
            }
        }
    }
    policies {
        from-zone trust to-zone trust {
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone untrust to-zone DMZ {
            /* policy for destination nat */
            policy WebServerPolicy {
                match {
                    source-address any;
                    /* WebServer from address book */
                    destination-address WebServer;
                    application junos-http;
                }
                then {
                    permit;
                }
            }
        }
        from-zone DMZ to-zone untrust {
            policy DMZ-internet-access {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone untrust to-zone trust {
            policy deny-any {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    deny;
                }
            }
        }
        from-zone trust to-zone untrust {
            policy internet-access {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
    }
    zones {
        security-zone DMZ {
            tcp-rst;
            address-book {
                address WebServer 192.168.4.10/32;
            }
            interfaces {
                ge-0/0/2.0 {
                    host-inbound-traffic {
                        system-services {
                            all;
                        }
                        protocols {
                            all;
                        }
                    }
                }
                ge-0/0/4.0 {
                    host-inbound-traffic {
                        system-services {
                            all;
                        }
                        protocols {
                            all;
                        }
                    }
                }
            }
        }
        security-zone trust {
            tcp-rst;
            interfaces {
                ge-0/0/1.0 {
                    host-inbound-traffic {
                        system-services {
                            all;
                        }
                        protocols {
                            all;
                        }
                    }
                }
            }
        }
        security-zone untrust {
            interfaces {
                ge-0/0/3.0 {
                    host-inbound-traffic {
                        system-services {
                            all;
                        }
                    }
                }
            }
        }
    }
}
firewall {
    filter my-port-filter {
        term myMirrorTerm {
            from {
                source-address {
                    0.0.0.0/0;
                }
            }
            then {
                port-mirror;
                accept;
            }
        }
    }
}
vlans {
    /* DMZ switch */
    vlan-DMZ {
        vlan-id 3;
        interface {
            ge-0/0/5.0;
        }
    }
    /* USERLAN switch */
    vlan-USERLAN {
        vlan-id 2;
        l3-interface vlan.0;
    }
}
