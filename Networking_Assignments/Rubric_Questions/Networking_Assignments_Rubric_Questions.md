# Assignment 15 Physical network with switch. 


1.	Regarding the network diagram with IP addresses with a brief explanation, mention something that your classmate did well 👍 and mention something that your classmate could improve at 📝

2.	Regarding showing a proof that the Raspberries are on the internet according to the diagram, mention something that your classmate did well 👍 and mention something that your classmate could improve at 📝

3.	Regarding pictures of the hardware setup. I.e. devices and cabling mention something that your classmate did well 👍 and mention something that your classmate could improve at 📝

5.	Regarding explanation of the switches fundamental job in a network, mention something that your classmate did well 👍 and mention something that your classmate could improve at 📝


# Assignment 29 Connecting VM Ware Workstation nets to physical nets.

1.	Regarding the HLD or Networking diagram and showing all interfaces designations, mention something that your classmate did well 👍 and mention something that your classmate could improve at 📝  

2.	Regarding the LLD design, mention something that your classmate did well 👍 and mention something that your classmate could improve at 📝

3.	Regarding demonstration of how Wireshark and Junos monitor traffic is used to monitor relevant network traffic, mention something that your classmate did well 👍 and mention something that your classmate could improve at 📝

4.	Regarding showing at least a virtual and a physical router routing tables and explaining very briefly the interesting parts, mention something that your classmate did well 👍 and mention something that your classmate could improve at 📝

5.	Regarding making the configurations available on GitLab, mention something that your classmate did well 👍 and mention something that your classmate could improve at 📝
