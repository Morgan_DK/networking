#!/usr/bin/python3
# By Per Dahlstrøm
# Very short program to demonstrate netmiko send_command
# Configuration mode is entered and show | display set
# is issued to list configuration set commands

import netmiko

myConnection = netmiko.ConnectHandler(ip='192.168.2.1',
                                      device_type='juniper',
                                      username='root',
                                      password='Rootpass')
myConnection.config_mode()
print(myConnection.send_command('show | display set'))
myConnection.exit_config_mode()
myConnection.disconnect()